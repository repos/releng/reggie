#!/usr/bin/env python3

import hashlib
import json
import logging
import os
import queue
import random
import requests
import sys
import tempfile
import threading
import time
import urllib.parse

# FIXME: cli
NUM_THREADS = 50
NUM_UPLOADS = 500
BLOB_SIZE = 1024 * 1024 * 20
UPLOAD_CHUNK_SIZE = 1024 * 1024
REGISTRY = "http://localhost:5000"


class Blob:
    def __init__(self, size=BLOB_SIZE):
        self.size = size
        self.file = None
        self.digest = None

        self._generate()

    def _generate(self):
        f = self.file = tempfile.TemporaryFile()
        h = hashlib.new("sha256")

        count = 0
        chunk_size = 1024 * 1024
        while count < self.size:
            data = random.randbytes(chunk_size)
            h.update(data)
            f.write(data)
            count += chunk_size
        f.flush()

        self.digest = "sha256:" + h.hexdigest()

    def get_stream(self):
        self.file.seek(0)
        return self.file

    def close(self):
        self.file.close()
        self.file = None


def upload_blob(blob):
    r = requests.post(os.path.join(REGISTRY, "v2/myrepo/blobs/uploads/"))
    r.raise_for_status()
    assert r.status_code == 202
    # FIXME: This needs to be a proper url merge since the location can be
    # absolute or relative.
    upload_url = REGISTRY + r.headers["location"]

    f = blob.get_stream()
    while True:
        got = f.read(UPLOAD_CHUNK_SIZE)
        if not got:
            break
        r = requests.patch(
            upload_url, data=got, headers={"Content-Type": "application/octet-stream"}
        )
        r.raise_for_status()
        assert r.status_code == 204
        upload_url = REGISTRY + r.headers["location"]

    # Wrap it up
    r = requests.put(upload_url, params={"digest": blob.digest})
    r.raise_for_status()
    assert r.status_code == 204


def upload_manifest(blob, name):
    manifest = {
        "mediaType": "application/vnd.docker.distribution.manifest.v2+json",
        "schemaVersion": 2,
        "config": {
            "digest": blob.digest,
        },
        "layers": [
            {
                "mediaType": "application/vnd.docker.image.rootfs.diff.tar.gzip",
                "digest": blob.digest,
            }
        ],
    }

    r = requests.put(
        os.path.join(REGISTRY, "v2/myrepo/manifests", urllib.parse.quote(name)),
        data=json.dumps(manifest),
        headers={"Content-Type": "application/json"},
    )
    r.raise_for_status()


def generate_tag():
    return str(time.time())


errors = 0
errors_lock = threading.Lock()


def inc_error():
    global errors

    with errors_lock:
        errors += 1


def do_operation():
    start = time.time()

    blob = Blob()
    end_make_blob = time.time()

    try:
        upload_blob(blob)
        end_upload_blob = time.time()

        upload_manifest(blob, blob.digest)
        end_upload_manifest_1 = time.time()

        upload_manifest(blob, generate_tag())
        end_upload_manifest_2 = time.time()

        upload_manifest(blob, "latest")
        end_upload_manifest_3 = time.time()

        elapsed = time.time() - start

        stats = {
            "total": elapsed,
            "make-blob": end_make_blob - start,
            "upload-blob": end_upload_blob - end_make_blob,
            "upload-manifest1": end_upload_manifest_1 - end_upload_blob,
            "upload-manifest2": end_upload_manifest_2 - end_upload_manifest_1,
            "upload-manifest3": end_upload_manifest_3 - end_upload_manifest_2,
        }

        logging.info(f"Operation stats:\n{json.dumps(stats, indent=2)}")
        # FIXME: use a stats package to summarize the stats at the end.
    except Exception as e:
        elapsed = time.time() - start
        logging.error(f"Operation failed after {elapsed} seconds:\n{e}")
        inc_error()
    finally:
        blob.close()


def worker(q):
    while True:
        try:
            q.get(block=False)
            do_operation()
        except queue.Empty:
            return


def main():
    logging.basicConfig(format="%(asctime)s %(message)s")
    logging.getLogger().setLevel(logging.INFO)

    q = queue.Queue()

    logging.info(
        f"Queuing {NUM_UPLOADS} {BLOB_SIZE}-byte uploads. Upload chunk size: {UPLOAD_CHUNK_SIZE}"
    )
    for n in range(NUM_UPLOADS):
        q.put(n)

    logging.info(f"Starting {NUM_THREADS} threads")
    start = time.time()
    threads = []
    for n in range(NUM_THREADS):
        thread = threading.Thread(target=worker, args=[q])
        thread.start()
        threads.append(thread)

    logging.info("Waiting for threads to terminate")
    for thread in threads:
        thread.join()
    elapsed = time.time() - start
    logging.info("All threads terminated")

    total_size = BLOB_SIZE * NUM_UPLOADS
    logging.info(
        f"""
Operation summary:
{NUM_UPLOADS} {BLOB_SIZE}-byte uploads completed in {elapsed} seconds.
{errors} errors.
{elapsed/NUM_UPLOADS} secs/upload.
{int(total_size/elapsed/1024/1024)} MiB bytes/sec.
"""
    )

    if errors:
        sys.exit(1)


main()
