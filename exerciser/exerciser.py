#!/usr/bin/python3

import subprocess
import threading
import time

# Grower:
#  Repeatedly create layers of a particular size and push to the latest tag (and perhaps a unique tag)
#  This will steadily create garbage.
#  As many of these as desired can run simultaneously.

# image_name must not contain a tag
# Size is in megabytes


def grow(image_name, size=100, use_timestamp_tag=False):
    Dockerfile = f"""
FROM debian:10
RUN echo "(C) {time.ctime()} Growth Industries" > README
RUN dd if=/dev/urandom of=/bloat.img bs=1024k count={size}
"""
    fqins = [image_name + ":latest"]
    if use_timestamp_tag:
        fqins.append(image_name + ":" + time.strftime("%Y%m%d-%H%M%S"))

    t_args = []
    for fqin in fqins:
        t_args.extend(["-t", fqin])

    subprocess.run(
        ["docker", "build", *t_args, "-"],
        input=Dockerfile,
        check=True,
        universal_newlines=True,
    )
    for fqin in fqins:
        subprocess.run(["docker", "push", fqin], check=True)
        # FIXME: This is sketchy with multiple threads.  We could
        # remove the 'latest' image which another thread might be just
        # about to push.
        subprocess.run(["docker", "rmi", fqin], check=True)


def grower(image_name, size=100, use_timestamp_tag=False):
    while True:
        grow(image_name, size, use_timestamp_tag)


# Pruner:
# Periodically scan for "old" tags and delete their manifests.


num_threads = 1


def main():
    threads = [
        threading.Thread(target=grower, args=("localhost:5000/exerciser/target",))
        for n in range(num_threads)
    ]

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()


if __name__ == "__main__":
    main()
