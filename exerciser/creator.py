# Fire off multiple threads all trying to create an upload in a newly
# created repo.  If db locking semantics are not correct, this will
# result in integrity constraint violation errors.

import uuid
import requests

from concurrent.futures import ThreadPoolExecutor

# 2 is sufficient to demonstrate
num_threads = 2
num_attempts = 1000


def initiate_upload(name):
    res = requests.post(f"http://localhost:5000/v2/{name}/blobs/uploads/")
    res.raise_for_status()
    return res


def attempt(pool):
    name = str(uuid.uuid4())
    futures = [pool.submit(initiate_upload, name) for n in range(num_threads)]

    exceptions = []

    for future in futures:
        e = future.exception()
        if e:
            exceptions.append(e)

    if exceptions:
        raise (SystemExit(f"Exceptions: {exceptions}"))


def main():
    with ThreadPoolExecutor(max_workers=num_threads) as pool:
        for n in range(num_attempts):
            print(f"Attempt #{n}")
            attempt(pool)


if __name__ == "__main__":
    main()
