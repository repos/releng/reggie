import json
import logging
import os
import shutil
import subprocess
import sys
import threading
import time
from http.server import BaseHTTPRequestHandler, HTTPServer

import jwt
import pytest
import requests
from jwcrypto import jwk

# i.e, one up from this module's dir
sys.path.insert(0, os.path.dirname(os.path.dirname(__file__)))

DEFAULT_JWT_ISSUER = "reggie-tests"


def pytest_configure():
    pytest.jwks_handler_issuer = DEFAULT_JWT_ISSUER


@pytest.fixture
def reggie(request, tmpdir, jwks_server):
    logger = logging.getLogger()

    datadir = os.path.join(tmpdir, "reggie-data")

    jwt = request.node.get_closest_marker("jwt")

    logger.info("Starting Reggie%s", " (JWT configuration)" if jwt else "")

    env = {"REGGIE_DATA_DIR": str(datadir)}
    if jwt:
        env.update(
            {
                "REGGIE_JWT_ENABLED": "true",
                "REGGIE_JWT_DECODE_ISSUERS": "reggie-tests,alternate-reggie-tests",
                "REGGIE_JWKS_ENDPOINT": jwks_server + "/oauth/discovery/keys",
                "REGGIE_AUTH_REALM": "reggie-tests",
            }
        )

    p = subprocess.Popen([shutil.which("flask"), "run"], env=env)

    wait_for_reggie()

    yield "http://127.0.0.1:5000"

    logger.info("Stopping Reggie")
    p.terminate()
    p.wait()


def wait_for_reggie():
    # Values in seconds
    polling_interval = 0.500
    timeout = 10

    logger = logging.getLogger()
    logger.info("Waiting for Reggie start")

    give_up_time = time.time() + timeout
    while time.time() < give_up_time:
        try:
            r = requests.get("http://127.0.0.1:5000/v2/")
            if r.content == b"{}\n":
                logger.info("Reggie started")
                return
        except requests.exceptions.ConnectionError:
            pass

        time.sleep(polling_interval)

    raise Exception(f"Reggie did not become accessible within {timeout} seconds")


class MyHTTPServer(HTTPServer):
    def __init__(self, server_address, RequestHandlerClass):
        super().__init__(server_address, RequestHandlerClass)

        self.jwt_key = jwk.JWK.generate(kty="RSA", size=2048, kid="test-key-id")
        self.private_pem = self.jwt_key.export_to_pem(private_key=True, password=None)


class JWKSHandler(BaseHTTPRequestHandler):
    def do_generate(self):
        now = int(time.time())
        exp = now + 10 * 60

        payload = {
            "iss": pytest.jwks_handler_issuer,
            "iat": now,
            "nbf": now,
            "exp": exp,
            "sub": "talking about you",
            "project_path": "testrepo",
        }

        self.send_response(200)
        self.send_header("Content-Type", "text/plain")
        self.end_headers()
        resp = jwt.encode(
            payload, self.server.private_pem, "RS256", {"kid": "test-key-id"}
        )
        self.wfile.write(resp.encode("UTF-8"))

    def do_GET(self):
        if self.path == "/generate":
            self.do_generate()
            return

        if self.path == "/oauth/discovery/keys":
            self.send_response(200)
            self.send_header("Content-Type", "application/json")
            self.end_headers()
            resp = {"keys": [self.server.jwt_key.export_public(as_dict=True)]}
            self.wfile.write(json.dumps(resp).encode("UTF-8"))
            return

        # Default
        self.send_response(404)
        self.send_header("Content-Type", "text/plain")
        self.end_headers()
        self.wfile.write(b"Not Found")


@pytest.fixture
def jwks_server():
    httpd = MyHTTPServer(("127.0.0.1", 0), JWKSHandler)
    server_thread = threading.Thread(target=httpd.serve_forever)
    server_thread.start()
    host, port = httpd.server_address
    yield f"http://{host}:{port}"
    httpd.shutdown()
    server_thread.join()
