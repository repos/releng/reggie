import base64
import hashlib
import io
import json
import pytest
import random
import requests
import urllib.parse

import reggie.cas.filesystem
import app
from tests.conftest import DEFAULT_JWT_ISSUER


def compute_digest(data: str) -> str:
    return "sha256:" + hashlib.sha256(data.encode("UTF-8")).hexdigest()


def upload_blob(reggie, data: str) -> str:
    """
    Returns the digest of the uploaded blob
    """

    digest = compute_digest(data)

    r = requests.post(reggie + "/v2/testrepo/blobs/uploads/")
    assert r.status_code == 202
    assert r.text == ""
    location = r.headers["location"]
    assert location
    uuid = r.headers["Docker-Upload-UUID"]
    assert uuid
    assert r.headers["range"] == "bytes=0-0"

    r = requests.put(reggie + location, params={"digest": digest}, data=data)
    assert r.status_code == 201
    assert r.text == ""
    assert r.headers["location"]
    assert r.headers["Docker-Content-Digest"] == digest
    assert r.headers["Content-Range"] == f"0-{len(data)-1}"

    return digest


# A bare-minimum manifest that will pass the validator.
sample_manifest = """
{
    "mediaType": "application/vnd.oci.image.index.v1+json",
    "schemaVersion": 2,
    "manifests": []
}
"""


def upload_test_manifest(reggie, manifest, tag):
    r = requests.put(
        reggie + "/v2/testrepo/manifests/" + tag,
        data=manifest,
        headers={"content-type": "application/json"},
    )

    assert r.status_code == 201
    assert r.headers.get("Docker-Content-Digest") == compute_digest(manifest)


def test_manifest_upload(reggie):
    upload_test_manifest(reggie, sample_manifest, "tag1")


def test_manifest_list_upload(reggie):
    config1 = json.dumps({"name": "Bogus config1"})
    layer1 = json.dumps({})
    config2 = json.dumps({"name": "Bogus config2"})
    layer2 = json.dumps({})

    config1_digest = upload_blob(reggie, config1)
    layer1_digest = upload_blob(reggie, layer1)
    config2_digest = upload_blob(reggie, config2)
    layer2_digest = upload_blob(reggie, layer2)

    # https://docs.docker.com/registry/spec/manifest-v2-2/
    manifest1 = {
        "schemaVersion": 2,
        "mediaType": "application/vnd.docker.distribution.manifest.v2+json",
        "config": {
            "mediaType": "application/vnd.docker.container.image.v1+json",
            "size": len(config1),
            "digest": config1_digest,
        },
        "layers": [
            {
                "mediaType": "application/vnd.docker.image.rootfs.diff.tar.gzip",
                "size": len(layer1),
                "digest": layer1_digest,
            }
        ],
    }
    manifest2 = {
        "schemaVersion": 2,
        "mediaType": "application/vnd.docker.distribution.manifest.v2+json",
        "config": {
            "mediaType": "application/vnd.docker.container.image.v1+json",
            "size": len(config2),
            "digest": config2_digest,
        },
        "layers": [
            {
                "mediaType": "application/vnd.docker.image.rootfs.diff.tar.gzip",
                "size": len(layer2),
                "digest": layer2_digest,
            }
        ],
    }

    manifest1 = json.dumps(manifest1)
    manifest1_digest = upload_blob(reggie, manifest1)
    manifest2 = json.dumps(manifest2)
    manifest2_digest = upload_blob(reggie, manifest2)

    manifest_list = {
        "schemaVersion": 2,
        "mediaType": "application/vnd.docker.distribution.manifest.list.v2+json",
        "manifests": [
            {
                "mediaType": "application/vnd.docker.distribution.manifest.v2+json",
                "size": len(manifest1),
                "digest": manifest1_digest,
                "platform": {"architecture": "amd64", "os": "linux"},
            },
            {
                "mediaType": "application/vnd.docker.distribution.manifest.v2+json",
                "size": len(manifest2),
                "digest": manifest2_digest,
                "platform": {"architecture": "arm64", "os": "linux"},
            },
        ],
    }
    manifest_list = json.dumps(manifest_list)

    upload_test_manifest(reggie, manifest_list, compute_digest(manifest_list))


@pytest.fixture
def jwt(request, jwks_server) -> str:
    if hasattr(request, "param"):
        pytest.jwks_handler_issuer = request.param
    else:
        pytest.jwks_handler_issuer = DEFAULT_JWT_ISSUER

    r = requests.get(jwks_server + "/oauth/discovery/keys")
    assert r.status_code == 200
    jwks = r.json()
    keys = jwks.get("keys")
    assert keys
    assert len(keys) == 1
    assert keys[0]["kid"] == "test-key-id"

    r = requests.get(jwks_server + "/generate")
    assert r.status_code == 200

    return r.content.decode("UTF-8")


@pytest.mark.jwt
def test_jwt_initiate_upload(reggie, jwt):
    # First try without supplying the auth header.
    r = requests.post(reggie + "/v2/testrepo/blobs/uploads/")
    assert r.status_code == 401
    assert (
        r.headers.get("www-authenticate")
        == 'Bearer realm="reggie-tests",service="Reggie",scope="repository:testrepo:pull,push"'
    )

    # Now try with the header
    r = requests.post(
        reggie + "/v2/testrepo/blobs/uploads/",
        headers={"Authorization": f"Bearer {jwt}"},
    )

    assert r.status_code == 202

    # Attempting to upload to a different repo should fail
    r = requests.post(
        reggie + "/v2/notmyrepo/blobs/uploads/",
        headers={"Authorization": f"Bearer {jwt}"},
    )

    assert r.status_code == 403


@pytest.mark.jwt
@pytest.mark.parametrize(
    "jwt", ["reggie-tests", "alternate-reggie-tests", "invalid_issuer"], indirect=True
)
def test_jwt_issuers(reggie, jwt):
    r = requests.post(
        reggie + "/v2/testrepo/blobs/uploads/",
        headers={"Authorization": f"Bearer {jwt}"},
    )

    if pytest.jwks_handler_issuer == "invalid_issuer":
        assert r.status_code == 422
    else:
        assert r.status_code == 202


def test_filesystem_upload(tmpdir):
    cas = reggie.cas.filesystem.FilesystemCAS(tmpdir)

    assert cas.have("bogus") is False

    uuid, upload_state = cas.initiate_upload()

    upload_state = cas.append_upload(uuid, upload_state, io.BytesIO(b"foo"))
    with pytest.raises(reggie.cas.filesystem.UploadOutOfSync) as e:
        cas.append_upload(uuid, 1, io.BytesIO(b"out-of-order"))
        assert e.expected_offset == 3

    upload_state = cas.append_upload(uuid, upload_state, io.BytesIO(b"bar"))

    digest = cas.finish_upload(uuid, upload_state)

    assert digest == "sha256:" + hashlib.sha256(b"foobar").hexdigest()

    assert cas.have(digest) is True


def generate_random_tag():
    while True:
        tag_len = random.randrange(1, 15)
        tag = base64.b64encode(random.randbytes(tag_len)).decode("UTF-8")

        if app.valid_tag(tag):
            return tag


def get_tags_paginated(reggie):
    limit = 2
    tags = []

    url = f"/v2/testrepo/tags/list?n={limit}"

    while True:
        r = requests.get(urllib.parse.urljoin(reggie, url))
        assert r.status_code == 200

        res = r.json()

        assert res.get("name") == "testrepo"

        returned_tags = res.get("tags")

        assert len(returned_tags) == limit

        tags.extend(returned_tags)

        link = r.headers.get("link")
        if not link:
            break
        url = link

    return tags


def test_tags_pagination(reggie):
    num_tags = 20

    tags = []
    for n in range(num_tags):
        tag = generate_random_tag()
        upload_test_manifest(reggie, sample_manifest, tag)
        tags.append(tag)

    tags.sort()

    received_tags = get_tags_paginated(reggie)

    assert received_tags == tags
