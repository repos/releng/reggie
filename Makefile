IMAGE = localhost:5000/reggie:latest

build:
	DOCKER_BUILDKIT=1 docker build -f .pipeline/blubber.yaml --target image -t $(IMAGE) .

FLASK_RUN_HOST ?= 0.0.0.0

run: build
	mkdir -p data
	docker run \
		--name reggie \
		--rm -it \
		--user $$(id -u):$$(id -g) \
		-v $$(pwd):/srv/app:ro \
		-v $$(pwd)/data:/data:rw \
		-p 5000:5000 \
		-e FLASK_RUN_HOST=$(FLASK_RUN_HOST) \
		-e FLASK_DEBUG -e FLASK_RUN_CERT -e FLASK_RUN_KEY \
		-e REGGIE_JWT_ENABLED -e REGGIE_JWKS_ENDPOINT -e REGGIE_AUTH_REALM -e JWT_DECODE_ISSUER \
		$(IMAGE)

run-debug:
	$(MAKE) run FLASK_DEBUG=true

push:
	docker push $(IMAGE)

clean-data:
	rm -fr data/db.sqlite data

BLANK_IMAGE = localhost:5000/blank

build-blank-image:
	docker build -t $(BLANK_IMAGE) -f Dockerfile.blank .

push-blank-image: build-blank-image
	docker push $(BLANK_IMAGE)

pushy:
	while docker push $(BLANK_IMAGE); do : ; done

####

lint: lint-using-docker

lint-local:
	tox

LINT_IMAGE = localhost:5000/reggie/lint:latest

lint-image:
	DOCKER_BUILDKIT=1 docker build --target lint -t $(LINT_IMAGE) -f .pipeline/blubber.yaml .

lint-using-docker: lint-image
	docker run --rm $(LINT_IMAGE)

reformat:
	tox -e reformat

####

TEST_IMAGE = localhost:5000/reggie/test:latest

test-image:
	DOCKER_BUILDKIT=1 docker build --target test -t $(TEST_IMAGE) -f .pipeline/blubber.yaml .

test: test-image
	docker run --rm $(TEST_IMAGE)

SMOKETEST_IMAGE = localhost:5000/reggie/smoketest:latest

smoketest:
	DOCKER_BUILDKIT=1 docker build --target smoketest -t $(SMOKETEST_IMAGE) -f .pipeline/blubber.yaml .
	docker run --rm $(SMOKETEST_IMAGE)

CONFORMANCE_TEST_IMAGE = localhost:5000/reggie/conformance-test:latest

conformance-test-image:
	DOCKER_BUILDKIT=1 docker build --target conformance -t $(CONFORMANCE_TEST_IMAGE) -f .pipeline/blubber.yaml .

conformance-test: build conformance-test-image
	docker rm -f reggie
	docker run --name reggie \
		-d \
		--rm \
		--expose 5000 \
		-e REGGIE_DATA_DIR=/tmp/data \
		-e FLASK_RUN_HOST=0.0.0.0 \
		$(IMAGE)
	sleep 1 # FIXME: Bleh
	trap "docker logs reggie ; docker rm -f reggie" EXIT && docker run --rm \
		--link reggie \
		--entrypoint /conformance.test \
		-e OCI_ROOT_URL='http://reggie:5000' \
		-e OCI_NAMESPACE='myorg/myrepo' \
		-e OCI_CROSSMOUNT_NAMESPACE='myorg/other' \
		-e OCI_TAG_NAME='mytag' \
		-e OCI_TEST_PULL=1 \
		-e OCI_TEST_PUSH=1 \
		-e OCI_TEST_CONTENT_DISCOVERY=1 \
		-e OCI_TEST_CONTENT_MANAGEMENT=1 \
		-e ACK_GINKGO_DEPRECATIONS=1.16.5 \
		$(CONFORMANCE_TEST_IMAGE)

