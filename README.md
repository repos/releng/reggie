![Reggie](Reggie-400x400.png)

# Reggie

Reggie is a container registry implemented in Python.  Its main feature is that it supports online garbage collection of deleted blobs.

To run the registry:
```sh
make run
```

To run the registry with debugging enabled:
```sh
make run-debug
```

# Configuration

Reggie is configured via environment variables. These are all documented below:

| Name              | Required? | Default | Description
|-------------------|-----------|---------|---------
| `REGGIE_DATA_DIR` | Yes       | `/data` | Location to store shared blobs & uploads. Additionally, the SQLite database is stored here by default.
| `REGGIE_CONNECTION_STRING` | Yes | `sqlite://${REGGIE_DATA_DIR}/db.sqlite` | Connection string to pass to SQLAlchemy.
| `REGGIE_MANIFEST_TTL` | No | `0` | Manifests older than this number of seconds will automatically be deleted.  0 disables automatic deletion.
| `REGGIE_MANIFEST_CLEANUP_INTERVAL` | No | `60` | Run manifest cleanup at the specified interval (in seconds).
| `REGGIE_JWT_ENABLED` | No  | | Whether to enable JWT authentication
| `REGGIE_AUTH_REALM` | No  | | The realm to use in the WWW-Authenticate header of HTTP 401 responses when REGGIE_JWT_ENABLED is true
| `REGGIE_JWKS_ENDPOINT` | No | | Required if `REGGIE_JWT_ENABLED` is true
| `REGGIE_JWT_DECODE_ISSUERS` | No | | Required if `REGGIE_JWT_ENABLED` is set. Comma-separated list of accepted issuers for the JWT

# Creating a new release

1. Visit https://gitlab.wikimedia.org/repos/releng/reggie/-/tags and
click on the blue `New tag` button to create a new tag.  Follow the naming pattern of the existing tags.
1. Creating the tag will trigger a pipeline that will build and publish a new Reggie container image.  Wait for the pipeline to complete.

# Deployment

## Deploying a new version

1. Create + merge MR to bump the `version` and `appVersion` in `reggie/helm/Chart.yaml`
2. In the `gitlab-cloud-runner` repository, ensure the `image.tag` property in [^reggie/reggie-values.yaml.tftpl] is set
to an empty string so the version from the helm chart is used:
   1. If `image.tag` is already an empty string: Manually start a new pipeline from the `main` branch using the
   "Run pipeline" button at https://gitlab.wikimedia.org/repos/releng/gitlab-cloud-runner/-/pipelines
   2. If `image.tag` is not an empty string: Create + merge MR in `gitlab-cloud-runner` to update `image.tag` to an
   empty string. A new pipeline will be automatically started upon merge
3. The pipeline will stop at manual step `deploy-staging`. Click on it to deploy to the staging environment 
4. Verify your changes in the staging environment: https://registry.staging.cloud.releng.team
5. Finally, click on the manual step `push-to-production`

The workflow above can also be used to deploy the version in `appVersion` (presumably the latest version) without
creating a new one. In that case simply skip step 1.

## Deploying a specific version (e.g. for a rollback)

1. Create + merge MR in the `gitlab-cloud-runner` repository to update the `image.tag` property in
[^reggie/reggie-values.yaml.tftpl] to the desired version
2. The `gitlab-cloud-runner` repo will launch a pipeline from `main`. The pipeline will stop at manual step `deploy-staging`.
Click on it to deploy to the staging environment
3. Verify your changes in the staging environment: https://registry.staging.cloud.releng.team
4. Finally, click on the manual step `push-to-production`

[^reggie/reggie-values.yaml.tftpl]: https://gitlab.wikimedia.org/repos/releng/gitlab-cloud-runner/-/blob/main/reggie/reggie-values.yaml.tftpl

