import tempfile
import os
import contextlib
import pickle
import base64


def base64_encode(data: bytes) -> str:
    """
    Convert bytes into a base64 string
    """
    return base64.b64encode(data).decode("ASCII")


def pickle64(thing) -> str:
    """
    Returns a base64 string representing the bytes resulting from pickling THING.
    """
    return base64_encode(pickle.dumps(thing))


def unpickle64(string):
    return pickle.loads(base64.b64decode(string))


@contextlib.contextmanager
def safe_write(final_filename):
    """
    safe_write yields a text stream on a temporary file
    that is open for writing.  If the context body completes without
    exception, the temp file is renamed to `final_filename`,
    atomically replacing any existing file of that name.  If an exception
    is raised during the exception of the body, the temp file is deleted
    and `final_filename` remains unaffected.

    Example:

    with safe_write("/tmp/important") as f:
        f.write("Important information")

    """

    # Create the temp file in the same directory as the final filename
    # so that os.rename() can atomically replace the destination file
    # (if one exists)
    with tempfile.NamedTemporaryFile(
        "w", dir=os.path.dirname(final_filename), delete=False
    ) as tmp:
        try:
            yield tmp
        except BaseException as e:
            os.unlink(tmp.name)
            raise e

    # Reach here on success
    os.chmod(tmp.name, 0o644)
    # This is atomic
    os.rename(tmp.name, final_filename)
