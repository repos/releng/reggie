import datetime

from sqlalchemy import Column, ForeignKey, Integer, String, Boolean
from sqlalchemy.orm import relationship
from sqlalchemy.types import DateTime

from .database import Base


class Blob(Base):
    __tablename__ = "blob"

    name = Column(String, primary_key=True)  # The digest
    created = Column(DateTime, nullable=False, default=datetime.datetime.now)
    size = Column(Integer, nullable=False)
    refcount = Column(Integer, nullable=False)


class GarbageBlob(Base):
    """
    This is a list of blobs that were deleted since the last GC operation.
    """

    __tablename__ = "garbageblob"

    name = Column(String, primary_key=True)  # The digest
    created = Column(DateTime, nullable=False, default=datetime.datetime.now)


class Repo(Base):
    __tablename__ = "repo"

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, index=True, unique=True)
    created = Column(DateTime, nullable=False, default=datetime.datetime.now)
    manifests = relationship("Manifest")
    tags = relationship("Tag")
    uploads = relationship("Upload")


class Manifest(Base):
    __tablename__ = "manifest"

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)  # The digest
    created = Column(DateTime, nullable=False, default=datetime.datetime.now)
    repo_id = Column(Integer, ForeignKey(Repo.id), nullable=False)
    contenttype = Column(String, nullable=False)
    # The number of tags pointing to this manifest
    refcount = Column(Integer, nullable=False, default=0)

    referenced_blobs = relationship("ManifestBlobReference", cascade="all, delete")


class Tag(Base):
    __tablename__ = "tag"

    created = Column(DateTime, nullable=False, default=datetime.datetime.now)
    repo_id = Column(Integer, ForeignKey(Repo.id), primary_key=True)
    name = Column(String, primary_key=True)

    manifest_id = Column(Integer, ForeignKey(Manifest.id), nullable=False, index=True)


class Upload(Base):
    __tablename__ = "upload"

    uuid = Column(String, primary_key=True)
    created = Column(DateTime, nullable=False, default=datetime.datetime.now)
    completed = Column(DateTime)
    repo_id = Column(Integer, ForeignKey(Repo.id), nullable=False)
    name = Column(String)  # The digest, populated when upload has completed


# This table is effectively a list of the unique blobs that have ever
# been uploaded to a given repo.
class RepoBlobReference(Base):
    __tablename__ = "repoblobreference"

    created = Column(DateTime, nullable=False, default=datetime.datetime.now)
    repo_id = Column(Integer, ForeignKey(Repo.id), primary_key=True)
    blob_name = Column(String, primary_key=True)
    deleted = Column(Boolean, default=False)


class ManifestBlobReference(Base):
    __tablename__ = "manifestblobreference"

    id = Column(Integer, primary_key=True)
    manifest_id = Column(Integer, ForeignKey(Manifest.id), nullable=False)
    blob_name = Column(String, nullable=False)


class Lock(Base):
    __tablename__ = "lock"

    id = Column(Integer, primary_key=True)
