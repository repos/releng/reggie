import io

from .models import Blob, Repo, RepoBlobReference

from reggie.cas.blobstore import BlobStore


class RepoBlobs:
    def __init__(self, db_session, blobstore: BlobStore):
        self.db_session = db_session
        self.blobstore = blobstore

    def initiate_upload(self):
        return self.blobstore.initiate_upload()

    def get_upload_offset(self, uuid):
        return self.blobstore.get_upload_offset(uuid)

    def append_upload(self, uuid, offset, stream):
        return self.blobstore.append_upload(uuid, offset, stream)

    def finish_upload(self, repo, uuid, offset) -> Blob:
        """
        The caller is expected to have acquired exclusive database access first.
        """
        blob = self.blobstore.finish_upload(uuid, offset)

        ref = self._get_ref(repo, blob.name)
        if not ref:
            # Update the list of blobs known to this repo.
            # print(f"Adding {blob.name} to access list of repo {repo.name}")
            ref = RepoBlobReference(repo_id=repo.id, blob_name=blob.name)
            self.db_session.add(ref)
        elif ref.deleted:
            # Revive a previously deleted blob ref
            ref.deleted = False
        else:
            # This blob has been uploaded before.
            pass

        return blob

    def put(self, repo, data: bytes) -> Blob:
        """
        The caller is expected to have acquired exclusive database access first.
        """
        uuid, offset = self.initiate_upload()
        offset = self.append_upload(uuid, offset, io.BytesIO(data))
        return self.finish_upload(repo, uuid, offset)

    def _get_ref(self, repo: Repo, digest) -> RepoBlobReference:
        return (
            self.db_session.query(RepoBlobReference)
            .where(RepoBlobReference.repo_id == repo.id)
            .where(RepoBlobReference.blob_name == digest)
            .scalar()
        )

    def have(self, repo: Repo, digest) -> bool:
        ref = self._get_ref(repo, digest)
        return ref and not ref.deleted

    def delete(self, repo: Repo, digest) -> bool:
        """
        Revokes access to the specified blob.

        If the blob is in the access list, remove it and return True.

        Otherwise return False.
        """

        ref = self._get_ref(repo, digest)
        if not ref or ref.deleted:
            return False

        ref.deleted = True
        return True

    def get(self, repo: Repo, digest) -> Blob:
        if not self.have(repo, digest):
            # This repo does not have access to the requested blob
            return None

        # This repo has access to the requested blob. Return it
        return self.blobstore.get_blob(digest)

    def get_data(self, repo: Repo, digest) -> bytes:
        if not self.have(repo, digest):
            # This repo does not have access to the requested blob
            return None

        # This repo has access to the requested blob. Return the contents.
        return self.blobstore.get(digest)

    def get_data_stream(self, repo: Repo, digest):
        """
        If 'repo' has access to the blob named by 'digest', returns
        (data_stream, length), otherwise returns (None, None).
        """

        if not self.have(repo, digest):
            # This repo does not have access to the requested blob
            return (None, None)

        return self.blobstore.get_stream(digest)
