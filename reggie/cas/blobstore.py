from sqlalchemy import exists

from ..database import lock_database_exclusive
from ..models import Blob, GarbageBlob
from .filesystem import FilesystemCAS

import logging


class BlobStore:
    def __init__(self, directory, session, digest_algorithm="sha256"):
        self.underlying_cas = FilesystemCAS(directory, digest_algorithm)
        self.session = session

    def have(self, digest) -> bool:
        return self.session.query(exists().where(Blob.name == digest)).scalar()

    def get_blob(self, digest) -> Blob:
        # FIXME: There might be a 'get' method that makes this more concise
        return self.session.query(Blob).where(Blob.name == digest).scalar()

    def get_stream(self, digest):
        """
        If the blob exists, return a byte stream and the length of the blob.
        If the blob does not exist, return (None, None)
        """
        if self.have(digest):
            return self.underlying_cas.get_stream(digest)
        return (None, None)

    def get(self, digest) -> bytes:
        stream, length = self.get_stream(digest)
        if not stream:
            return None

        try:
            return stream.read()
        finally:
            stream.close()

    def initiate_upload(self):
        return self.underlying_cas.initiate_upload()

    def get_upload_offset(self, uuid):
        return self.underlying_cas.get_upload_offset(uuid)

    def append_upload(self, uuid, offset, stream):
        return self.underlying_cas.append_upload(uuid, offset, stream)

    def finish_upload(self, uuid, offset) -> Blob:
        digest = self.underlying_cas.finish_upload(uuid, offset)
        return self._maybe_add_blob(digest, offset)

    def _maybe_add_blob(self, digest, size) -> Blob:
        blob = self.get_blob(digest)

        if not blob:
            blob = Blob(name=digest, size=size, refcount=0)
            self.session.add(blob)

            status = None
            gb = (
                self.session.query(GarbageBlob)
                .where(GarbageBlob.name == digest)
                .scalar()
            )
            if gb:
                status = "revived from the dead"
                self.session.delete(gb)
            else:
                status = "new"

            logging.getLogger().debug(
                "blob %s added to blobstore (%s)", blob.name, status
            )

        return blob

    def add_ref(self, blob: Blob):
        blob.refcount += 1
        logging.getLogger().debug(
            "Refcount of blob %s advanced to %s", blob.name, blob.refcount
        )

    def drop_ref(self, blob: Blob):
        blob.refcount -= 1
        logging.getLogger().debug(
            "Refcount of blob %s dropped to %s", blob.name, blob.refcount
        )

        if blob.refcount > 0:
            return

        if blob.refcount < 0:
            raise Exception(
                f"refcount of blob {blob.name} dropped below zero to {blob.refcount}.  This should never happen"
            )

        # The last reference has been dropped.  Delete the blob from
        # the main table and add it to the GC table.
        logging.getLogger().debug(
            "Deleting blob %s from table and marking for GC", blob.name
        )
        self.session.add(GarbageBlob(name=blob.name))
        self.session.delete(blob)

    def drop_ref_by_name(self, digest):
        blob = self.get_blob(digest)
        assert blob
        self.drop_ref(blob)

    def gc(self):
        logging.getLogger().debug("** blobstore gc start")
        lock_database_exclusive(self.session)
        for gb in self.session.query(GarbageBlob):
            logging.getLogger().debug("** gc deleting file for %s", gb.name)
            self.underlying_cas.delete(gb.name)
            self.session.delete(gb)

        logging.getLogger().debug("** blobstore gc end")

        self.session.commit()
