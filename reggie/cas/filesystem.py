import os
from pathlib import Path
from uuid import uuid4
import rehash
import time
import json
import reggie.utils
import shutil
from prometheus_client import Gauge


class FilesystemCAS:
    """
    This class implements a content addressable store backed by the filesystem.
    """

    def __init__(self, directory, digest_algorithm="sha256"):
        self.directory = directory
        self.digest_algorithm_name = digest_algorithm
        self.digest_algorithm = getattr(rehash, digest_algorithm)
        self.diskUsage = Gauge(
            "reggie_filesystem_bytes_used",
            "The number of bytes stored in the filesystem content addressable store, excluding in-progress uploads",
        )
        self.diskUsage.set(self._compute_usage())

    def _filename(self, digest) -> str:
        fullpath = Path(self.directory, digest).resolve()
        if not fullpath.is_relative_to(self.directory):
            raise Exception("Relative paths not permitted for storage")
        return str(fullpath)

    ###

    def have(self, digest) -> bool:
        return os.path.exists(self._filename(digest))

    def get_stream(self, digest):
        """
        Returns a byte stream and the length of the file.

        The caller is responsible for closing the stream.
        """
        filename = self._filename(digest)
        size = os.path.getsize(filename)
        stream = open(filename, "rb")

        return stream, size

    def initiate_upload(self) -> str:
        """
        Returns the uuid of the upload, and the offset that must be
        passed to the next append_upload() or finish_upload() call.
        """

        while True:
            uuid = str(uuid4())

            upload_dir = self._upload_dir(uuid)

            try:
                os.mkdir(upload_dir)
                break
            except FileExistsError:
                # Loop around and try a different uuid
                pass

        open(self._upload_file(uuid), "w").close()

        state = {
            "startedAt": time.time(),
            "updatedAt": 0,
            "lastOffset": 0,
            "digestState": self.digest_algorithm(),
        }

        self._update_upload_state_file(uuid, state)

        return uuid, 0

    def get_upload_offset(self, uuid):
        state = self._read_upload_state_file(uuid)

        return state["lastOffset"]

    def append_upload(self, uuid, offset, stream):
        state = self._read_upload_state_file(uuid)

        if offset != state["lastOffset"]:
            print(
                f'append_upload: Got offset {offset} but expected {state["lastOffset"]}'
            )
            raise UploadOutOfSync(state["lastOffset"])

        with open(self._upload_file(uuid), "wb") as f:
            f.truncate(offset)
            f.seek(offset)

            chunksize = 1024 * 1024
            while True:
                got = stream.read(chunksize)
                if not got:
                    break
                f.write(got)
                state["digestState"].update(got)
                offset += len(got)

        state["lastOffset"] = offset

        self._update_upload_state_file(uuid, state)

        return offset

    def finish_upload(self, uuid, offset) -> str:
        """
        Returns the digest of the blob.
        """
        state = self._read_upload_state_file(uuid)

        if offset != state["lastOffset"]:
            raise UploadOutOfSync(state)

        digest = self.digest_algorithm_name + ":" + state["digestState"].hexdigest()

        if not self.have(digest):
            os.rename(self._upload_file(uuid), self._filename(digest))
            # FIXME: fdatasync the file and the parent directory for max
            # durability guarantees.
            self.diskUsage.inc(offset)

        shutil.rmtree(self._upload_dir(uuid))

        return digest

    def _upload_dir(self, uuid):
        return os.path.join(self.directory, uuid)

    def _upload_state_file(self, uuid):
        return os.path.join(self._upload_dir(uuid), "state")

    def _upload_file(self, uuid):
        return os.path.join(self._upload_dir(uuid), "blob")

    def _update_upload_state_file(self, uuid, state):
        state = state.copy()
        state["digestState"] = reggie.utils.pickle64(state["digestState"])
        state["updatedAt"] = time.time()

        with reggie.utils.safe_write(self._upload_state_file(uuid)) as f:
            json.dump(state, f)

    def _read_upload_state_file(self, uuid):
        with open(self._upload_state_file(uuid)) as f:
            state = json.load(f)

        state["digestState"] = reggie.utils.unpickle64(state["digestState"])

        return state

    def delete(self, digest) -> bool:
        if not self.have(digest):
            return False

        filename = self._filename(digest)
        size = os.path.getsize(filename)
        os.unlink(filename)
        self.diskUsage.dec(size)

        return True

    def _scan(self):
        # `finish_upload` defines the filename format.
        prefix = self.digest_algorithm_name + ":"

        for entry in os.scandir(self.directory):
            if entry.name.startswith(prefix):
                yield entry.path

    def _compute_usage(self) -> int:
        total = 0
        for filename in self._scan():
            total += os.path.getsize(filename)
        return total


class UploadOutOfSync(Exception):
    def __init__(self, expected_offset):
        self.expected_offset = expected_offset
