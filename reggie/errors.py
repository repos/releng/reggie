class RegistryError(Exception):
    http_code = 500
    error_code = "UNKNOWN_ERROR"
    message = "Unknown error"

    def __init__(self, message, http_code=None, error_code=None):
        self.message = message
        if http_code:
            self.http_code = http_code
        if error_code:
            self.error_code = error_code


class InvalidRepoNameError(RegistryError):
    http_code = 400
    error_code = "NAME_INVALID"


class UnknownRepoError(RegistryError):
    http_code = 404
    error_code = "NAME_UNKNOWN"


class InvalidReferenceError(RegistryError):
    http_code = 404
    error_code = "MANIFEST_UNKNOWN"


class UnknownManifestError(RegistryError):
    http_code = 404
    error_code = "MANIFEST_UNKNOWN"


class UnsupportedManifestTypeError(RegistryError):
    http_code = 400
    error_code = "MANIFEST_INVALID"


class InvalidManifestError(RegistryError):
    http_code = 400
    error_code = "MANIFEST_INVALID"


class UnknownBlobError(RegistryError):
    http_code = 400
    error_code = "BLOB_UNKNOWN"


class ForbiddenError(RegistryError):
    http_code = 403
    error_code = "DENIED"
