from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker, declarative_base

Base = declarative_base()

from . import models  # noqa: E402


def init_db(connection_string, timeout):
    engine = create_engine(
        connection_string, echo=False, connect_args={"timeout": timeout}
    )
    db_session = scoped_session(
        sessionmaker(autocommit=False, autoflush=False, bind=engine)
    )

    Base.metadata.create_all(bind=engine)

    return db_session


def lock_database_exclusive(db_session):
    """
    Put the SQLite database into exclusive access mode by performing an insert
    """
    lock = models.Lock()
    db_session.add(lock)
    db_session.flush()
    db_session.delete(lock)
