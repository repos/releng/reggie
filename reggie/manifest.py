from .errors import InvalidManifestError, UnknownBlobError, UnsupportedManifestTypeError
from .models import Blob


class ManifestValidator:
    def __init__(self, repoblobs, repo, manifest):
        self.repoblobs = repoblobs
        self.repo = repo
        self.manifest = manifest

    # xref https://docs.docker.com/registry/spec/manifest-v2-2/
    def _validate_vnd_docker_distribution_manifest_v2(self):
        schemaVersion = self.manifest.get("schemaVersion")
        if schemaVersion is None:
            raise InvalidManifestError("Missing schemaVersion")
        if schemaVersion != 2:
            raise InvalidManifestError("schemaVersion must be 2")

        config = self.manifest.get("config")
        if config is None:
            raise InvalidManifestError("Missing config")

        blobs = []
        blobs.append(self._validate_config(config))

        layers = self.manifest.get("layers")
        if layers is None:
            raise InvalidManifestError("Missing layers")

        for layer in layers:
            blobs.append(self._validate_layer(layer))

        return blobs

    def _get_blob(self, digest):
        return self.repoblobs.get(self.repo, digest)

    def _validate_config(self, config) -> Blob:
        """
        Returns the Blob for the config
        """
        digest = config.get("digest")
        if digest is None:
            raise InvalidManifestError("config digest is missing")

        blob = self._get_blob(digest)
        if not blob:
            raise UnknownBlobError(f"Config blob {digest} is unknown to the repo")

        return blob

    def _validate_layer(self, layer) -> Blob:
        """
        Returns the Blob for the layer
        """
        mediatype = layer.get("mediaType")
        if mediatype != "application/vnd.docker.image.rootfs.diff.tar.gzip":
            raise UnsupportedManifestTypeError(
                "Layer mediaType {} is not supported".format(mediatype)
            )

        digest = layer.get("digest")
        if digest is None:
            raise UnsupportedManifestTypeError("Layer is missing digest field")

        blob = self._get_blob(digest)
        if not blob:
            raise UnknownBlobError(
                "Layer blob {} is unknown to the repo".format(digest)
            )

        return blob

    def _validate_index_and_list_common(self):
        schemaVersion = self.manifest.get("schemaVersion")
        if schemaVersion is None:
            raise InvalidManifestError("Missing schemaVersion")

        if schemaVersion != 2:
            raise InvalidManifestError("schemaVersion must be 2")

        manifests = self.manifest.get("manifests")
        if manifests is None:
            raise InvalidManifestError("Missing inner manifests")

        blobs = []

        for inner in manifests:
            digest = inner.get("digest")
            if digest is None:
                raise InvalidManifestError("Inner manifest missing digest field")

            blob = self._get_blob(digest)
            if not blob:
                raise UnknownBlobError("Unknown blob referenced")

            blobs.append(blob)

        return blobs

    # xref
    # https://github.com/opencontainers/image-spec/blob/main/image-index.md
    def _validate_vnd_oci_image_index_v1(self):
        return self._validate_index_and_list_common()

    # xref https://docs.docker.com/registry/spec/manifest-v2-2/
    def _validate_vnd_docker_distribution_manifest_list_v2(self):
        return self._validate_index_and_list_common()

    # xref https://github.com/opencontainers/image-spec/blob/main/config.md
    def _validate_vnd_oci_image_config_v1(self):
        return []

    VALIDATORS = {
        "application/vnd.docker.distribution.manifest.v2+json": _validate_vnd_docker_distribution_manifest_v2,
        "application/vnd.oci.image.index.v1+json": _validate_vnd_oci_image_index_v1,
        "application/vnd.oci.image.config.v1+json": _validate_vnd_oci_image_config_v1,
        "application/vnd.docker.distribution.manifest.list.v2+json": _validate_vnd_docker_distribution_manifest_list_v2,
    }

    def validate(self) -> list:
        """
        Returns a list of Blobs referenced by the manifest
        """

        mediatype = self.manifest.get("mediaType") or self.manifest.get(
            "config", {}
        ).get("mediaType")
        if mediatype is None:
            raise InvalidManifestError("Missing mediaType")

        validator = self.VALIDATORS.get(mediatype)
        if validator is None:
            raise UnsupportedManifestTypeError(
                f"Manifest mediaType {mediatype} is not supported"
            )

        return validator(self)
