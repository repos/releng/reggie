#!/usr/bin/python3
import sys
from functools import wraps
import logging
import os
import re
from pathlib import Path
import datetime
import threading
import time

import sqlalchemy.exc
from flask import Flask, request, send_file, url_for
from flask_jwt_extended import JWTManager, verify_jwt_in_request, get_jwt
from werkzeug.middleware.dispatcher import DispatcherMiddleware
from jwt import PyJWKClient, InvalidIssuerError
from cryptography.hazmat.primitives import serialization
import prometheus_client

from reggie.cas.blobstore import BlobStore
from reggie.cas.filesystem import UploadOutOfSync
from reggie.database import init_db, lock_database_exclusive
from reggie.errors import (
    InvalidReferenceError,
    InvalidRepoNameError,
    RegistryError,
    UnknownManifestError,
    UnknownRepoError,
    ForbiddenError,
)
from reggie.manifest import ManifestValidator
from reggie.models import Manifest, ManifestBlobReference, Repo, Tag, Upload
from reggie.repo import RepoBlobs

app = Flask(__name__)
db_session = None

datadir = Path(os.getenv("REGGIE_DATA_DIR", "/data"))
app.config["REGGIE_BLOBS_DIR"] = datadir / "shared-blobs"
app.config["REGGIE_CONNECTION_STRING"] = os.getenv(
    "REGGIE_CONNECTION_STRING", "sqlite:///{}".format(datadir / "db.sqlite")
)
app.config["REGGIE_DB_TIMEOUT"] = int(os.getenv("REGGIE_DB_TIMEOUT", "5"))
app.config["REGGIE_MANIFEST_TTL"] = int(os.getenv("REGGIE_MANIFEST_TTL", "0"))
app.config["REGGIE_MANIFEST_CLEANUP_INTERVAL"] = int(
    os.getenv("REGGIE_MANIFEST_CLEANUP_INTERVAL", "60")
)
app.config["REGGIE_JWT_ENABLED"] = str(os.getenv("REGGIE_JWT_ENABLED", "")).lower() in [
    "yes",
    "y",
    "on",
    "true",
    "1",
]
app.config["REGGIE_AUTH_REALM"] = os.getenv(
    "REGGIE_AUTH_REALM", "REGGIE_AUTH_REALM not set"
)
app.config["REGGIE_JWKS_ENDPOINT"] = os.getenv("REGGIE_JWKS_ENDPOINT")
app.config["REGGIE_DEBUG_JWT"] = os.getenv("REGGIE_DEBUG_JWT", "").lower() in [
    "yes",
    "y",
    "on",
    "true",
    "1",
]
app.config["REGGIE_JWT_DECODE_ISSUERS"] = [
    iss.strip()
    for iss in os.getenv("REGGIE_JWT_DECODE_ISSUERS", "").split(",")
    if iss.strip()
]
app.config["JWT_IDENTITY_CLAIM"] = "project_path"
app.config["JWT_ISSUER"] = "iss"

if app.config["REGGIE_DEBUG_JWT"] and not len(app.config["REGGIE_JWT_DECODE_ISSUERS"]):
    logging.error(
        "jwt verification is enabled but no list of valid issuers was specified"
    )
    sys.exit(1)

app.config["JWT_DECODE_ALGORITHMS"] = ["RS256"]

jwt = JWTManager(app)


def jwt_required():
    def wrapper(fn):
        @wraps(fn)
        def decorator(*args, **kwargs):
            if not app.config["REGGIE_JWT_ENABLED"]:
                return fn(*args, **kwargs)

            debug = app.config["REGGIE_DEBUG_JWT"]

            # All routes that use @jwt_required have a first argument
            # named 'name' which is the repo portion of the URL.
            reponame = kwargs["name"]

            if debug:
                logging.info(f"debug jwt: Request headers: {request.headers!r}")

            auth_header = request.headers.get("Authorization")
            if not auth_header:
                if debug:
                    logging.info("debug jwt: Missing Authorization header")
                details = [
                    {
                        "Type": "repository",
                        "Name": reponame,
                        "Action": action,
                    }
                    for action in ["push", "pull"]
                ]

                res = {
                    "errors": [
                        {
                            "code": "UNAUTHORIZED",
                            "message": "authentication required",
                            "detail": details,
                        }
                    ]
                }

                return (
                    res,
                    401,
                    {
                        "WWW-Authenticate": f'Bearer realm="{app.config["REGGIE_AUTH_REALM"]}",service="Reggie",scope="repository:{reponame}:pull,push"'
                    },
                )

            try:
                verify_jwt_in_request()
            except Exception as e:
                if debug:
                    logging.info(f"debug jwt: verify_jwt_in_request() failed: {e!r}")
                raise e

            jwt_payload = get_jwt()
            if debug:
                logging.info(f"debug jwt: Decoded JWT: {jwt_payload!r}")

            if (
                jwt_payload[app.config["JWT_ISSUER"]]
                not in app.config["REGGIE_JWT_DECODE_ISSUERS"]
            ):
                raise InvalidIssuerError("Invalid issuer")

            project_path = jwt_payload[app.config["JWT_IDENTITY_CLAIM"]]

            if request.path.startswith(f"/v2/{project_path}"):
                if debug:
                    logging.info("debug jwt: Access granted")
                return fn(*args, **kwargs)
            else:
                if debug:
                    logging.info(
                        f"debug jwt: Access denied (request path doesn't start with /v2/{project_path})"
                    )
                raise ForbiddenError("requested access to the resource is denied")

        return decorator

    return wrapper


@jwt.decode_key_loader
def decode_key_loader(jwt_header, _):
    jwks_client = PyJWKClient(app.config["REGGIE_JWKS_ENDPOINT"])
    jwk = jwks_client.get_signing_key(jwt_header["kid"])
    return jwk.key.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo,
    ).decode("UTF-8")


@app.teardown_appcontext
def shutdown_session(exception=None):
    if db_session:
        # This will first call Session.close() method on the current Session, which
        # releases any existing transactional/connection resources still being
        # held; transactions specifically are rolled back. The Session is then
        # discarded. Upon next usage within the same scope, the scoped_session will
        # produce a new Session object.
        # https://docs.sqlalchemy.org/en/20/orm/contextual.html#sqlalchemy.orm.scoped_session.remove
        db_session.remove()


def valid_repo_name(name):
    """
    1. A repository name is broken up into _path components_. A component of a
    repository name must be at least one lowercase, alpha-numeric characters,
    optionally separated by periods, dashes or underscores. More strictly, it
    must match the regular expression `[a-z0-9]+(?:[._-][a-z0-9]+)*`.
    2. If a repository  name has two or more path components, they must be
    separated by a forward slash ("/").
    3. The total length of a repository name, including slashes, must be less
    than 256 characters.
    https://docs.docker.com/registry/spec/api/#overview
    """

    if not name or len(name) >= 256:
        return False
    for comp in name.split("/"):
        if not re.fullmatch(r"[a-z0-9]+(?:[._-][a-z0-9]+)*", comp):
            return False
    return True


def get_repo(name) -> Repo:
    """
    No transaction is active on return.
    """
    if not valid_repo_name(name):
        raise InvalidRepoNameError("Invalid repo name: {}".format(name))

    with db_session.begin():
        repo = db_session.query(Repo).where(Repo.name == name).scalar()

    if repo is None:
        raise UnknownRepoError("Unknown repository: {}".format(name))

    return repo


def ensure_repo(name) -> Repo:
    """
    NOTES:
    * This will perform a commit if needed to create a new Repo.
    * No transaction is active on return.
    """
    try:
        repo = get_repo(name)
        return repo
    except UnknownRepoError:
        pass

    # Concurrency note: Another thread could be running ensure_repo at
    # the same time.  Both this thread and the other can reach this
    # point (seeing no existence of the repo) and try to create it.
    # Handle that possibility here.

    try:
        repo = Repo(name=name)
        db_session.add(repo)
        db_session.commit()
    except sqlalchemy.exc.IntegrityError:
        # FIXME: Move this stuff out of the exception handler somehow
        db_session.rollback()
        return ensure_repo(name)

    return repo


@app.errorhandler(RegistryError)
def handle_error(e):
    res = {"errors": [{"code": e.error_code, "message": e.message}]}
    return (res, e.http_code)


@app.after_request
def version_check(response):
    response.headers["docker-distribution-api-version"] = "registry/2.0"
    return response


@app.get("/v2/")
def v2():
    return {}


@app.get("/v2/_catalog")
def get_catalog():
    repo_names = []

    with db_session.begin():
        for repo in db_session.query(Repo):
            repo_names.append(repo.name)

    return {"repositories": repo_names}


@app.post("/v2/<path:name>/blobs/uploads/")
@jwt_required()
def initiate_upload(name):
    repo = ensure_repo(name)

    uuid, offset = REPOBLOBS.initiate_upload()

    with db_session.begin():
        lock_database_exclusive(db_session)
        upload = Upload(uuid=uuid, repo_id=repo.id)
        db_session.add(upload)
        db_session.commit()

    supplied_digest = request.args.get("digest")
    if supplied_digest:
        offset = REPOBLOBS.append_upload(uuid, offset, request.stream)
        return finish_upload(upload, repo, uuid, offset, supplied_digest)

    return upload_status_response(202, name, uuid, offset)


@app.get("/v2/<path:name>/blobs/uploads/<uuid>")
def get_upload_status(name, uuid):
    # Verify that the repo exists. We don't need the returned object
    get_repo(name)

    with db_session.begin():
        upload = db_session.query(Upload).get(uuid)
        if upload:
            offset = REPOBLOBS.get_upload_offset(uuid)

    if not upload:
        res = {
            "errors": [
                {"code": "BLOB_UPLOAD_UNKNOWN", "message": "Unknown upload uuid"}
            ]
        }
        return (res, 404)

    return upload_status_response(204, name, uuid, offset)


@app.route("/v2/<path:name>/blobs/uploads/<uuid>", methods=["PATCH", "PUT"])
@jwt_required()
def process_upload(name, uuid):
    repo = get_repo(name)

    offset = request.args.get("_offset", None)
    if offset is None:
        res = {"errors": [{"code": "BLOB_UPLOAD_UNKNOWN", "message": "Missing offset"}]}
        return (res, 400)
    try:
        offset = int(offset)
    except ValueError:
        res = {"errors": [{"code": "BLOB_UPLOAD_UNKNOWN", "message": "Invalid offset"}]}
        return (res, 400)

    with db_session.begin():
        upload = db_session.query(Upload).get(uuid)

    if not upload:
        res = {
            "errors": [
                {"code": "BLOB_UPLOAD_UNKNOWN", "message": "Unknown upload uuid"}
            ]
        }
        return (res, 404)

    content_range = request.headers.get("Content-Range")
    if content_range:
        expected_offset = REPOBLOBS.get_upload_offset(uuid)
        m = re.match(r"(\d+)-\d+$", content_range)
        if not m or int(m[1]) != expected_offset:
            return upload_status_response(416, name, uuid, expected_offset)

    try:
        offset = REPOBLOBS.append_upload(uuid, offset, request.stream)
    except UploadOutOfSync as e:
        return upload_status_response(416, name, uuid, e.expected_offset)

    if request.method == "PATCH":
        return upload_status_response(
            202, name, uuid, offset, include_range_units=False
        )

    # PUT
    supplied_digest = request.args.get("digest")
    if not supplied_digest:
        res = {
            "errors": [
                {"code": "DIGEST_INVALID", "message": "Missing digest query parameter"}
            ]
        }
        return (res, 400)

    return finish_upload(upload, repo, uuid, offset, supplied_digest)


def finish_upload(upload, repo, uuid, offset, supplied_digest):
    with db_session.begin():
        lock_database_exclusive(db_session)
        blob = REPOBLOBS.finish_upload(repo, uuid, offset)
        # The upload references the blob
        BLOBSTORE.add_ref(blob)

        computed_digest = blob.name
        upload.name = computed_digest
        upload.completed = datetime.datetime.now()
        db_session.commit()

    if supplied_digest != computed_digest:
        logging.getLogger().error(
            "supplied_digest %s, computed_digest %s", supplied_digest, computed_digest
        )
        res = {
            "errors": [
                {
                    "code": "DIGEST_INVALID",
                    "message": "Supplied digest does not match the computed digest",
                }
            ]
        }
        return (res, 400)

    logging.getLogger().debug(
        "Upload id %s finalized. Upload digest %s", uuid, computed_digest
    )

    return (
        "",
        201,
        {
            "Location": url_for("get_blob", name=repo.name, digest=computed_digest),
            "Content-Range": "0-{}".format(offset - 1),
            "Docker-Content-Digest": computed_digest,
        },
    )


def upload_status_response(code, name, uuid, offset, include_range_units=True):
    location = url_for("process_upload", name=name, uuid=uuid) + f"?_offset={offset}"

    # Note that the HTTP Range header byte ranges are inclusive.  This also means that you
    # can't tell the difference between an empty file and a file with one byte.
    if offset > 0:
        offset -= 1

    range = f"0-{offset}"
    if include_range_units:
        range = f"bytes={range}"

    return (
        "",
        code,
        {
            "location": location,
            "Docker-Upload-UUID": uuid,
            "range": range,
        },
    )


@app.get("/v2/<path:name>/blobs/<digest>")
def get_blob(name, digest):
    repo = get_repo(name)

    with db_session.begin():
        stream, length = REPOBLOBS.get_data_stream(repo, digest)
        if stream is None:
            res = {
                "errors": [{"code": "BLOB_UNKNOWN", "message": "blob unknown to repo"}]
            }
            return (res, 404)

    resp = send_file(stream, mimetype="application/octet-stream")
    resp.content_length = length

    return resp


@app.delete("/v2/<path:name>/blobs/<digest>")
@jwt_required()
def delete_blob(name, digest):
    """
    Revoke the repo's access to the blob.
    """
    logging.debug(f"delete_blob({name}, {digest})")
    repo = get_repo(name)

    with db_session.begin():
        lock_database_exclusive(db_session)
        if REPOBLOBS.delete(repo, digest):
            db_session.commit()
            return ("", 202)
        else:
            res = {
                "errors": [{"code": "BLOB_UNKNOWN", "message": "blob unknown to repo"}]
            }
            return (res, 404)


def valid_digest(digest):
    return re.fullmatch(r"[a-z0-9_+.-]+:[a-f0-9]+", digest, re.IGNORECASE)


def valid_tag(tag):
    """
    A tag name must be valid ASCII and may contain lowercase and
    uppercase letters, digits, underscores, periods and dashes. A tag
    name may not start with a period or a dash and may contain a
    maximum of 128 characters.
    https://docs.docker.com/engine/reference/commandline/tag/#description
    """
    if len(tag) > 128:
        return False

    return re.fullmatch(r"[a-z0-9_][a-z0-9_.-]+", tag, re.IGNORECASE)


def validate_reference(thing):
    if not valid_digest(thing) and not valid_tag(thing):
        raise InvalidReferenceError("Invalid reference {}".format(thing))


@app.get("/v2/<path:name>/tags/list")
def get_tags(name):
    repo = get_repo(name)

    limit = request.args.get("n")
    if limit:
        try:
            limit = int(limit)
        except ValueError:
            raise RegistryError(f"Invalid limit: {limit}", http_code=400)
        if limit < 1:
            raise RegistryError(f"Invalid limit: {limit}", http_code=400)

    last = request.args.get("last")

    # Tags are returned in sorted order so no need to sort here.
    with db_session.begin():
        tags = [tag.name for tag in repo.tags]

    if last:
        try:
            pos = tags.index(last)
            tags = tags[pos + 1 :]
        except ValueError:
            # It's unclear what is supposed to happen
            # if the 'last' value is not in the list.  For now
            # we treat that condition as if 'last' was not supplied.
            pass

    reached_limit = limit and len(tags) > limit
    if reached_limit:
        tags = tags[:limit]

    res = {
        "name": name,
        "tags": tags,
    }

    headers = {}

    if reached_limit:
        headers["Link"] = url_for("get_tags", name=name, n=str(limit), last=tags[-1])

    return (res, 200, headers)


@app.get("/v2/<path:name>/manifests/<reference>")
def get_manifest(name, reference):
    repo = get_repo(name)

    with db_session.begin():
        manifest = _get_manifest(repo, reference)

        return (
            REPOBLOBS.get_data(repo, manifest.name),
            200,
            {
                "Content-Type": manifest.contenttype,
                "Docker-Content-Digest": manifest.name,
            },
        )


@app.put("/v2/<path:name>/manifests/<reference>")
@jwt_required()
def put_manifest(name, reference):
    logging.info("put_manifest(%s, %s)", name, reference)

    repo = ensure_repo(name)
    validate_reference(reference)

    with db_session.begin():
        lock_database_exclusive(db_session)

        try:
            # Manifest validation queries the database for the existence of
            # blobs referenced by the manifest.
            blobs = ManifestValidator(REPOBLOBS, repo, request.json).validate()
        except Exception as e:
            logging.info("put_manifest(%s, %s) caught exception %s", name, reference, e)
            logging.info(f"Manifest was: {request.json}")
            raise

        manifest_blob = REPOBLOBS.put(repo, request.data)
        digest = manifest_blob.name

        manifest = None
        try:
            manifest = _get_manifest(repo, digest)
        except UnknownManifestError:
            pass

        if not manifest:
            logging.getLogger().debug("put_manifest: New manfest")
            manifest = Manifest(
                name=digest, repo_id=repo.id, contenttype=request.content_type
            )
            db_session.add(manifest)
            BLOBSTORE.add_ref(manifest_blob)

            # Flush so we can collect the new manifest's id (FIXME: Find a magic way of doing this.  I think we can
            # use the 'backpopulate' stuff).
            db_session.flush()
            for blob in blobs:
                db_session.add(
                    ManifestBlobReference(manifest_id=manifest.id, blob_name=blob.name)
                )
                BLOBSTORE.add_ref(blob)
        else:
            logging.getLogger().debug("put_manifest: Already have this manifest")

        if valid_tag(reference):
            tag = (
                db_session.query(Tag)
                .where(Tag.repo_id == repo.id)
                .where(Tag.name == reference)
                .scalar()
            )
            if tag:
                prior_manifest = db_session.query(Manifest).get(tag.manifest_id)
                assert prior_manifest

                if prior_manifest.name != digest:
                    logging.getLogger().debug(
                        "put_manifest: Tag %s updated to point to %s (formerly %s)",
                        reference,
                        digest,
                        prior_manifest.name,
                    )

                    prior_manifest.refcount -= 1

                    if prior_manifest.refcount < 0:
                        raise RegistryError(
                            "refcount of manifest %s dropped below 0 to %s.  This should never happen",
                            prior_manifest.name,
                            prior_manifest.refcount,
                        )
                    if prior_manifest.refcount == 0:
                        logging.getLogger().debug(
                            "Manifest %s has been orphaned (no more tags pointing to it)",
                            prior_manifest.name,
                        )

                    tag.manifest_id = manifest.id
                    manifest.refcount += 1
            else:
                logging.getLogger().debug(
                    "put_manifest: New tag %s pointing to %s", reference, digest
                )
                db_session.add(
                    Tag(name=reference, repo_id=repo.id, manifest_id=manifest.id)
                )
                manifest.refcount += 1

        db_session.commit()

    return (
        "",
        201,
        {
            "Docker-Content-Digest": digest,
            "Location": url_for("get_manifest", name=name, reference=reference),
        },
    )


@app.delete("/v2/<path:name>/manifests/<reference>")
@jwt_required()
def delete_manifest(name, reference):
    logging.debug(f"delete_manifest({name}, {reference})")
    # https://docs.docker.com/registry/spec/api/#delete-manifest
    if not valid_digest(reference):
        logging.debug("raising InvalidReferenceError")
        raise InvalidReferenceError(
            "Invalid reference {}. Manifests can only be deleted by digest".format(
                reference
            ),
            http_code=400,
            error_code="UNSUPPORTED",
        )

    repo = get_repo(name)

    with db_session.begin():
        lock_database_exclusive(db_session)
        _delete_manifest(_get_manifest(repo, reference))
        db_session.commit()
    BLOBSTORE.gc()

    return ("", 202)


# Reggie special API.


@app.get("/v2/<path:name>/manifests/untagged")
def get_untagged_manifests(name):
    repo = get_repo(name)

    with db_session.begin():
        return [manifest.name for manifest in _get_untagged_manifests(repo)]


# Reggie special API.


@app.delete("/v2/<path:name>/manifests/untagged")
@jwt_required()
def delete_untagged_manifests(name):
    return {"manifests_deleted": _delete_untagged_manifests(get_repo(name))}


def _get_manifest(repo, reference) -> Manifest:
    validate_reference(reference)

    if valid_digest(reference):
        manifest = (
            db_session.query(Manifest)
            .where(Manifest.repo_id == repo.id)
            .where(Manifest.name == reference)
            .scalar()
        )
        if manifest:
            return manifest
    else:
        tag = (
            db_session.query(Tag)
            .where(Tag.repo_id == repo.id)
            .where(Tag.name == reference)
            .scalar()
        )
        if tag:
            manifest = (
                db_session.query(Manifest)
                .where(Manifest.id == tag.manifest_id)
                .scalar()
            )
            if manifest:
                return manifest

    raise UnknownManifestError("Unknown manifest {}".format(reference))


def _delete_manifest(manifest: Manifest):
    logging.getLogger().debug("_delete_manifest(%s)", manifest.name)

    # Delete any tags that point to this manifest.
    # FIXME: Figure out how to do that automagically w/ relations
    for tag in db_session.query(Tag).where(Tag.manifest_id == manifest.id):
        db_session.delete(tag)

    for ref in manifest.referenced_blobs:
        BLOBSTORE.drop_ref_by_name(ref.blob_name)

    # Delete the Manifest object.  Its model is configured
    # to delete child ManifestBlobReference objects too.
    db_session.delete(manifest)

    BLOBSTORE.drop_ref_by_name(manifest.name)
    logging.getLogger().debug("_delete_manifest done")


def _get_untagged_manifests(repo: Repo):
    """
    Returns an iterable of Manifest objects
    """
    return (
        db_session.query(Manifest)
        .where(Manifest.repo_id == repo.id)
        .where(Manifest.refcount == 0)
    )


def _delete_untagged_manifests(repo: Repo) -> int:
    """
    Returns the number of manifests deleted
    """
    logging.getLogger().debug("delete_untagged_manifests(%s)", repo.name)

    with db_session.begin():
        lock_database_exclusive(db_session)

        count = 0
        for manifest in _get_untagged_manifests(repo):
            _delete_manifest(manifest)
            count += 1

        db_session.commit()
    BLOBSTORE.gc()

    logging.getLogger().debug("delete_untagged_manifests(%s) done", repo.name)

    return count


# FIXME: These need to be configurable
UPLOAD_CLEAN_INTERVAL = 60
COMPLETED_UPLOAD_TIMEOUT = datetime.timedelta(minutes=2)
INCOMPLETE_UPLOAD_TIMEOUT = datetime.timedelta(minutes=10)


def clean_uploads():
    any_drops = False

    with db_session.begin():
        lock_database_exclusive(db_session)
        for upload in db_session.query(Upload):
            if upload.name:
                # Completed upload
                if (
                    datetime.datetime.now() - upload.completed
                    >= COMPLETED_UPLOAD_TIMEOUT
                ):
                    logging.info(
                        f"Upload cleaner removed completed upload {upload.uuid} which finished at {upload.completed}"
                    )
                    BLOBSTORE.drop_ref_by_name(upload.name)
                    any_drops = True
                    db_session.delete(upload)
            else:
                # Incomplete upload
                if (
                    datetime.datetime.now() - upload.created
                    >= INCOMPLETE_UPLOAD_TIMEOUT
                ):
                    logging.info(
                        f"Upload cleaner removed incomplete upload {upload.uuid} which was created at {upload.created}"
                    )
                    db_session.delete(upload)

    if any_drops:
        BLOBSTORE.gc()


def upload_cleaner():
    while True:
        logging.info("Upload cleaner running")
        clean_uploads()
        logging.info("Upload cleaner finished")
        time.sleep(UPLOAD_CLEAN_INTERVAL)


def clean_manifests():
    any_deletions = False

    ttl_cutoff = datetime.datetime.utcnow() - datetime.timedelta(
        seconds=app.config["REGGIE_MANIFEST_TTL"]
    )

    with db_session.begin():
        lock_database_exclusive(db_session)
        for manifest in db_session.query(Manifest).where(Manifest.created < ttl_cutoff):
            repo = db_session.query(Repo).get(manifest.repo_id)
            assert repo
            logging.info(
                f"Manifest cleaner: Deleting {repo.name}/manifests/{manifest.name}"
            )
            # Deleting a manifest automatically deletes any tags pointing to it.
            _delete_manifest(manifest)
            any_deletions = True
        db_session.commit()

    if any_deletions:
        BLOBSTORE.gc()


def manifest_cleaner():
    while True:
        logging.info("Manifest cleaner running")
        clean_manifests()
        logging.info("Manifest cleaner finished")
        time.sleep(app.config["REGGIE_MANIFEST_CLEANUP_INTERVAL"])


# Add prometheus wsgi middleware to route /metrics requests
app.wsgi_app = DispatcherMiddleware(
    app.wsgi_app, {"/metrics": prometheus_client.make_wsgi_app()}
)

#########

# Filthy hack


def flask_run() -> bool:
    import inspect

    for frame in inspect.stack():
        if frame.function == "run_command":
            return True

    return False


if flask_run():
    logging.basicConfig(format="%(asctime)s %(message)s")
    logging.getLogger().setLevel(logging.INFO)

    for dir_type in ["REGGIE_BLOBS_DIR"]:
        dir = app.config[dir_type]
        try:
            os.makedirs(dir, exist_ok=True)
        except Exception as e:
            msg = f"Failed to create {dir}: {e}"

            if isinstance(e, PermissionError):
                msg += "\nSet REGGIE_DATA_DIR to a writable directory"

            raise SystemExit(msg)

    try:
        connection_string = app.config["REGGIE_CONNECTION_STRING"]
        db_session = init_db(connection_string, app.config["REGGIE_DB_TIMEOUT"])
    except Exception as e:
        raise SystemExit(
            f"Failed to initialize database with connection string '{connection_string}':\n{e}"
        )

    BLOBSTORE = BlobStore(app.config["REGGIE_BLOBS_DIR"], db_session)
    BLOBSTORE.gc()
    REPOBLOBS = RepoBlobs(db_session, BLOBSTORE)

    threading.Thread(target=upload_cleaner, daemon=True).start()
    if app.config["REGGIE_MANIFEST_TTL"]:
        threading.Thread(target=manifest_cleaner, daemon=True).start()

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
